from flask import Blueprint, request, redirect, render_template, url_for
from flask.json import jsonify as jsonify
from flask.views import MethodView
from jinja2 import TemplateNotFound
import logging
from facts import *
from tag.exceptions import *
import portalocker

logging.basicConfig(filename='logs/tag.log',level=logging.DEBUG)

views = Blueprint('pages',__name__, template_folder='templates')
tagfile = 'tagfacts'

def validate_usertoken(usertoken):
    userfile = open('userkeys','r')
    for user in userfile:
        print "handling %s" % user
        if usertoken == user.rstrip():
            return True
    raise AuthException

def unset_tag(tag):
    try:
        content = []
        with open(tagfile, 'r') as f:
            content = [line.rstrip('\n') for line in f]
        if tag in content:
            print "1"
            content.remove(tag)
            print "2"
            with open(tagfile, 'w') as f:
                print "3"
                portalocker.lock(f, portalocker.LOCK_EX)
                print "4"
                for item in content:
                    print "got %s" % item
                    f.write(item+"\n")
    except:
        raise
    return

def set_tag(tag):
    try:
        with open(tagfile, 'r+') as f:
            portalocker.lock(f, portalocker.LOCK_EX)
            content = [line.rstrip('\n') for line in f]
            if tag not in content:
                f.write(tag+"\n")
    except:
        raise
    return

class TagDelete(MethodView):
    def get(self, usertoken):
        try:
            validate_usertoken(usertoken)
        except Exception, message:
            return render_template('error.html', message=message)

        try:
            if request.args.get('tag'):
                unset_tag(request.args.get('tag'))
        except:
            raise
        return "OK", 200

class TagPut(MethodView):
    def get(self, usertoken):
        try:
            validate_usertoken(usertoken)
        except Exception, message:
            return render_template('error.html', message=message)

        try:
            if request.args.get('tag'):
                set_tag(request.args.get('tag'))
        except Exception, message:
            return (message, 500)
        return "OK", 200

class JsonFact(MethodView):
    def get(self, usertoken):
        try:
            validate_usertoken(usertoken)
        except Exception, message:
            return render_template('error.html', message=message)
        fact = {}

        # Get a single fact
        if request.args.get('fact'):
            fact=render_fact(request.args.get('fact'))

        # Get a view
        if request.args.get('view'):
            fact=render_view(request.args.get('view'))

        return (jsonify(fact), 200)

class Ping(MethodView):
    def get(self, usertoken):
        try:
            validate_usertoken(usertoken)
        except Exception, message:
            return render_template('error.html', message=message)

        return 'ping', 200

class Summary(MethodView):
    def get(self, usertoken):
        try:
            validate_usertoken(usertoken)
        except Exception, message:
            return render_template('error.html', message=message)

        facts = get_health_facts()

        return render_template('index.html', facts=facts, usertoken=usertoken)

class Local(MethodView):
    def get(self, usertoken):
        try:
            validate_usertoken(usertoken)
        except Exception, message:
            return render_template('error.html', message=message)

        facts = get_local_facts()

        return render_template('local.html', facts=facts, usertoken=usertoken)

class Facter(MethodView):
    def get(self, usertoken):
        try:
            validate_usertoken(usertoken)
        except Exception, message:
            return render_template('error.html', message=message)

        facts = get_facter_facts()

        return render_template('facter.html', facts=facts, usertoken=usertoken)

# Html Views
views.add_url_rule('/<usertoken>', view_func=Summary.as_view('summary'))
views.add_url_rule('/local/<usertoken>', view_func=Local.as_view('local'))
views.add_url_rule('/facter/<usertoken>', view_func=Facter.as_view('facter'))
views.add_url_rule('/ping/<usertoken>', view_func=Ping.as_view('ping'))

# Json views
views.add_url_rule('/jsonfact/<usertoken>', view_func=JsonFact.as_view('jsonfact'))
views.add_url_rule('/tagput/<usertoken>', view_func=TagPut.as_view('tagput'))
views.add_url_rule('/tagdelete/<usertoken>', view_func=TagDelete.as_view('tagdelete'))
#views.add_url_rule('/jsonfact/<usertoken>/<factq>', view_func=JsonFact.as_view('jsonfact'))
#views.add_url_rule('/health/<connect_id>', view_func=health.as_view('health'))
#views.add_url_rule('/facts/<connect_id>', view_func=health.as_view('facts'))

