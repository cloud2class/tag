class FactException(Exception):
    def __init__(self):
        Exception.__init__(self,"This Fact is either not defined or not         allowed")

class AuthException(Exception):
    def __init__(self):
        Exception.__init__(self,"Please provide a valid user-key")
