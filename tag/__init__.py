from flask import Flask, send_from_directory
import logging, ssl, os
import tag.exceptions

logging.basicConfig(filename='logs/tag.log',level=logging.DEBUG)

#app = Flask(__name__, static_folder="static", static_url_path='')
app = Flask(__name__, static_url_path='')

def register_blueprints(app):
    from tag.views import views
    app.register_blueprint(views)

register_blueprints(app)

if __name__ == '__main__':
    app.run(host='0.0.0.0',port='8000')

# Static paths
@app.route('/css/<path:path>')
def send_css(path):
    return send_from_directory(os.path.join(app.root_path, 'static/css'),path)
@app.route('/js/<path:path>')
def send_js(path):
    return send_from_directory(os.path.join(app.root_path, 'static/js'),path)
@app.route('/fonts/<path:path>')
def send_fonts(path):
    return send_from_directory(os.path.join(app.root_path, 'static/fonts'),path)
