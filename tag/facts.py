import sys, platform, os, psutil, facter, subprocess
from datetime import timedelta
from hurry.filesize import size, si

tagfile = 'tagfacts'

def get_facts(factlist):
    facts = {}
    for keys in factlist:
        try:
            facts[keys] = globals()["fact_"+keys]()
        except Exception, message:
            pass
    return facts

def render_view(view):
    result = {}

    try:
        viewfile = open((view+".view"),'r')
    except:
        return result

    checkfile = open('myfacts','r')
    mycheck = {}
    for check in checkfile:
        mycheck[check.rstrip()] = 1

    f = facter.Facter()
    for line in viewfile:
        myline = line.rstrip()
        try:
            result[myline] = globals()["fact_"+myline]()
        except:
            if mycheck[myline] == 1:
                result[myline] = f.lookup(myline)
    return result

def render_fact(fact):
    result = {}

    # Try local
    try:
        result[fact] = globals()["fact_"+fact]()
    except:
        checkfile = open('myfacts','r')
        for check in checkfile:
            check = check.rstrip()
            if check == fact:
                f = facter.Facter()
                result[fact] = f.lookup(fact)

    return result

def get_health_facts():
    factlist = ['hostname', 'platform', 'cpu_count_phys', 'cpu_count', 'disk', 'load_average', 'memory', 'network_connections', 'interfaces', 'architecture', 'uptime','primary_services','tags']
    return get_facts(factlist)

def get_local_facts():
    factlist = ['hostname', 'platform', 'cpu_count_phys', 'cpu_count', 'disk', 'load_average', 'memory', 'network_connections', 'interfaces', 'users', 'architecture', 'uptime', 'tags', 'primary_services']
    return get_facts(factlist)

def get_facter_facts():
    checkfile = open('myfacts','r')
    facts = {}
    facter_facts = facter.Facter()
    for check in checkfile:
        check = check.rstrip()

        try:
            facts[check] = facter_facts[check]
        except:
            pass
    return facts

# Local fact definitions

def fact_hostname():
    return platform.node()

def fact_architecture():
    return platform.architecture()

def fact_platform():
    return sys.platform

def fact_cpu_count_phys():
    return psutil.cpu_count(logical=False)

def fact_cpu_count():
    return psutil.cpu_count()

def fact_disk():
    facts = {}
    for disk in psutil.disk_partitions():
        disk_detail=psutil.disk_usage(disk.mountpoint)
        facts[disk.mountpoint] = {}
        facts[disk.mountpoint]['freepercent'] = disk_detail.percent
        facts[disk.mountpoint]['total'] = size(disk_detail.total)
    return facts

def fact_load_average():
    facts = {}
    myavg = os.getloadavg()
    facts['load_average_1'] = myavg[0]
    facts['load_average_5'] = myavg[1]
    facts['load_average_15'] = myavg[2]
    return facts

def fact_memory():
    facts = {}
    memory = psutil.virtual_memory()
    facts['memory_total'] = size(memory.total, system=si)
    facts['memory_used'] = size(memory.used, system=si)
    facts['memory_used_percent'] = size(memory.percent, system=si)
    facts['memory_used_buffer'] = size(memory.buffers, system=si)
    facts['memory_used_cached'] = size(memory.cached, system=si)
    return facts

def fact_network_connections():
    network = psutil.net_connections()
    return len('network')

def fact_interfaces():
    facts = {}
    interfaces = psutil.net_if_stats()
    for interface in interfaces.keys():
        facts[interface] = {}
        facts[interface]['status'] = interfaces[interface][0]
    return facts

def fact_users():
    return len(psutil.users())

def fact_uptime():
    with open('/proc/uptime', 'r') as f:
        uptime_seconds = float(f.readline().split()[0])
        uptime_string = str(timedelta(seconds = uptime_seconds))
        return(uptime_string)
    return

def fact_primary_services():
    facts = {}

    with open('services','r') as f:
        content = [line.rstrip('\n') for line in f]
        for item in content:
            facts[item] = {}
            facts[item]['running'] = False
            facts[item]['memory'] = 0
        for service in psutil.pids():
            detail = psutil.Process(service)
            if detail.name() in content:
                facts[detail.name()]['running'] = True
                facts[detail.name()]['memory'] = round(detail.memory_percent())
    return facts

def fact_tags():
    facts = []

    with open(tagfile,'r') as f:
        facts = [line.rstrip('\n') for line in f]
    return facts
