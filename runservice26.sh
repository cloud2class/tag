#!/bin/bash

echo Setting PYTHONPATH
export PYTHONPATH='python/lib/python2.6/site-packages'

echo Entering environment
. venv/bin/activate

echo Create logs directory, if it doesnt exist
mkdir logs 2>&1 > /dev/null

echo launching webserver
gunicorn -c gunicorn.conf tag:app -D

echo Done. Please check for errors
