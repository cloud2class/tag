#!/bin/bash

# Wheel installation for Python2.6
# Run this from the install root. e.g. /home/bob/tag, or /opt/tag

echo Creating Python directory
mkdir -p python/lib/python2.6/site-packages

echo Setting PYTHONPATH
installdir=$(pwd)
export PYTHONPATH="$installdir/python/lib/python2.6/site-packages"


echo Installing local virtualenv
tar zxvf virtualenv-13.1.0.tar.gz

cd virtualenv-13.1.0
python setup.py install --prefix=$installdir/python
cd ..

echo Installing local environment
python python/bin/virtualenv venv
. venv/bin/activate
pip install --use-wheel --no-index --find-links=wheelhouse26 -r requirements.txt

echo Done - Please check for errors
