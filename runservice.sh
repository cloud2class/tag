#!/bin/bash

echo Installing virtual environment
virtualenv venv
. venv/bin/activate
echo Installing local python modules
pip install -r requirements.txt
echo creating logging directory
mkdir logs 2>&1 > /dev/null
echo launching webserver
gunicorn -c gunicorn.conf tag:app -D

