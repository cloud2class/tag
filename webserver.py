#!/usr/bin/python

from flask import Flask, request
from flask.ext.script import Manager, Server
import logging
from tag import app

manager = Manager(app)

manager.add_command("runserver", Server(
    use_debugger = True,
    use_reloader = True,
    host = '0.0.0.0',
    port = '8000'))

if __name__ == "__main__":
    manager.run()

